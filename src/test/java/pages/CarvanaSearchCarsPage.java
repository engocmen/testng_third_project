package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.List;

public class CarvanaSearchCarsPage {

    public CarvanaSearchCarsPage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }



    @FindBy(css = "span[data-qa='label-text']")
    public List<WebElement> optionsFilterList;

    @FindBy(css = "input[data-qa='search-bar-input']")
    public WebElement searchInput;


    @FindBy(css = "picture[class='vehicle-image']")
    public List<WebElement> searchImagesList;

    @FindBy(css = "svg[class='favorite-icon']")
    public List<WebElement> favoriteButtonList;

    @FindBy(css = "div[class='result-tile']")
    public List<WebElement> tileBodyList;

    @FindBy(css = "div[class='tk-pane full-width']>div")
    public List<WebElement> inventoryListYearListTrimListDeliveryList;

    @FindBy(css = "div[class='terms']>div")
    public List<WebElement> paymentMonthlyAndDownList;

    @FindBy(css = "div[data-testid='price']")
    public List<WebElement> price;

}
